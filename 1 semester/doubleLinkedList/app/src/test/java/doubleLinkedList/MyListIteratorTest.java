package doubleLinkedList;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;


class MyListIteratorTest {
	static MyList list;

	@BeforeAll
 	static void createList() {
    list = new MyList("A");
		list.add("B");
		list.add("C");
		list.add("A");
  }

	@Test void getCurrentTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator();
		assertEquals(myIterator.getCurrent(), "A");
	}

	@Test void nextTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator();
		assertEquals(myIterator.next(), "A");
		assertEquals(myIterator.getCurrent(), "B");
	}

	@Test void previousTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator(1);
		assertEquals(myIterator.previous(), "B");
		assertEquals(myIterator.getCurrent(), "A");
	}

	@Test void addTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator();
		myIterator.add("N");
		assertEquals(list.toString(), "My list: A, N, B, C, A, ");
	}

	@Test void removeTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator();
		myIterator.remove();
		assertEquals(list.toString(), "My list: B, C, A, ");
	}

	@Test void nextIndexTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator(1);
		assertEquals(myIterator.nextIndex(), 2);
	}

	@Test void previousIndexTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator(1);
		assertEquals(myIterator.previousIndex(), 0);
	}

	@Test void isNotEmptyTest(){
		MyList.MyListIterator myIterator = (MyList.MyListIterator) list.listIterator(1);
		assertTrue(myIterator.isNotEmpty());
		MyList newList = new MyList("A");
		newList.remove("A");
		myIterator = (MyList.MyListIterator) newList.listIterator();
		assertTrue(!(myIterator.isNotEmpty()));

	}
}
