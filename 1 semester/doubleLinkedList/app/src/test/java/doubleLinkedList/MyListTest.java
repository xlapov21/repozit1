package doubleLinkedList;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;


class MyListTest {
	static MyList list;

	@BeforeAll
 	static void createList() {
    list = new MyList("A");
		list.add("B");
		list.add("C");
		list.add("A");
  }

	@Test void containsTest(){
		assertTrue(!(list.contains("V")));
		assertTrue(list.contains("C"));
	}

	@Test void containsAllTest(){
		MyList subList = (MyList) list.subList(1, 2);
		MyList enderList = new MyList("A");
		enderList.add("V");
		assertTrue(list.containsAll(subList));
		assertTrue(!(list.containsAll(enderList)));
	}

	@Test void getTest(){
		assertEquals(list.get(2), "C");
	}

	@Test void indexOfTest(){
		assertEquals(list.indexOf("A"), 0, "1 element found uncorrect");
		assertEquals(list.indexOf("B"), 1, "2 element found uncorrect");
		assertEquals(list.indexOf("C"), 2, "2 element found uncorrect");
		assertEquals(list.indexOf("V"), -1, "enemy element found uncorrect");
	}

	@Test void lastIndexOfTest(){
		assertEquals(list.lastIndexOf("A"), 3, "1 element found uncorrect");
		assertEquals(list.lastIndexOf("B"), 1, "2 element found uncorrect");
		assertEquals(list.lastIndexOf("C"), 2, "2 element found uncorrect");
		assertEquals(list.lastIndexOf("V"), -1, "enemy element found uncorrect");
	}

	@Test void listIteratorTest(){
		assertEquals(((MyList.MyListIterator) list.listIterator()).getCurrent(), "A");
		assertEquals(((MyList.MyListIterator) list.listIterator(2)).getCurrent(), "C");
	}

	@Test void subListTest(){
		MyList subList = (MyList) list.subList(1, 2);
		assertEquals(subList.toString(), "My list: B, C, ");
	}

	@Test void toArrayTest(){
		Object[] array = list.toArray();
		assertEquals(array.length, 4);
		assertEquals(array[0], "A");
		assertEquals(array[1], "B");
		assertEquals(array[2], "C");
		assertEquals(array[3], "A");
	}

	@Test void isEmptyTest(){
		MyList subList = new MyList("A");
		subList.remove("A");
		assertTrue(!(list.isEmpty()));
		assertTrue(subList.isEmpty());
	}

	@Test void toStringTest(){
		assertEquals(list.toString(), "My list: A, B, C, A, ");
	}

	@Test void removeTest(){
		list.remove(2);
		assertEquals(list.toString(), "My list: A, B, A, ");
		list.remove("A");
		assertEquals(list.toString(), "My list: B, A, ");
	}

	@Test void addTest(){
		list.add(2, "B");
		assertEquals(list.toString(), "My list: A, B, C, B, A, ");
		list.add("B");
		assertEquals(list.toString(), "My list: A, B, C, B, A, B, ");
	}

	@Test void sizeTest(){
		assertEquals(list.size(), 4);
	}








}
