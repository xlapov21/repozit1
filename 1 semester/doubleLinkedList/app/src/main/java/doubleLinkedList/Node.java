package doubleLinkedList;

class Node<T>{
	private T data;
	private Node nextItem;
	private Node previousItem;

	Node(T data){this.data = data;}

	void setNextItem(Node nextItem){ this.nextItem = nextItem;}

	void setPreviousItem(Node previousItem){ this.previousItem = previousItem;}

	void setData(T data){
		if (data != null){
			this.data = data;
		}
		else{
			throw new NullPointerException();
		}
	}

	Node getNextItem(){ return nextItem;}

	Node getPreviousItem(){ return previousItem;}

	T getData(){ return data;}
}
