package doubleLinkedList;

import java.util.*;

class MyList<E> implements List<E>{

	private Node<E> head;

	/**
	* Конструктор MyList. При создании экземпляра сразу задается нулевой элемент
	*/
	MyList(E obj){
		head = new Node<>(obj);
	}

	/**
	* Проверяет наличие элемента в массиве.
	*/
	public boolean contains(Object o){
		System.out.println(indexOf(o) != -1);
		return indexOf(o) != -1;
	}

	/**
	* Проверяет принадлежность указонного массива к этому
	*/
	public	boolean containsAll(Collection c){
		if (c == null || !(c instanceof MyList)){
			return false;
		}
		else {
			MyListIterator<E> cIterator = (MyListIterator<E>) (((MyList<E>) c).listIterator());
			for (int i = 0; i > ((MyList<E>) c).size(); i++){
				if (!(contains(((MyList<E>) c).get(i)))){
					return false;
				}
			}
			return true;
		}
	}

	/**
	* Возвращает объект, хранящийся в указанном индексе в вызывающей коллекции.
	*/
	public E get(int index){
		checkIndex(index);
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(index);
		return myIterator.getCurrent();
	}

	/**
	* Возвращает индекс первого экземпляра obj в вызывающем списке.
	Если obj не является элементом списка, возвращается -1.
	*/
	public int indexOf(Object obj){
		int size = size();
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator();
		for (int i = 0; i < size - 1; i++){
			if (myIterator.next().equals(obj)){
				return i;
			}
		}
		if (myIterator.getCurrent().equals(obj)){
			return (size - 1);
		}
		else{
			return -1;
		}
	}

	/**
	* Возвращает индекс последнего экземпляра obj в вызывающем списке.
	Если obj не является элементом списка, возвращается -1.
	*/
	public int lastIndexOf(Object obj){
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(size()-1);
		for (int i = size() - 1; i > 0; i--){
			if (myIterator.previous().equals(obj)){
				return i;
			}
		}
		if (myIterator.getCurrent().equals(obj)){
			return (0);
		}
		else{
			return -1;
		}
	}

	/**
	* Возвращает итератор в началe списка.
	*/
	public ListIterator listIterator(){
		MyListIterator<E> myIterator = new MyListIterator<>(head);
		return myIterator;
	}

	/**
	* Возвращает итератор на индексе
	*/
	public ListIterator listIterator(int index){
		checkIndex(index);
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator();
		for (int i = 0; i < index; i++){
			myIterator.next();
		}
		return myIterator;
	}


	/**
	* Возвращает список, содержащий элементы от start до end
	*/
	public List subList(int start, int end){
		checkIndex(start);
		checkIndex(end);
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(start);
		MyList<E> newList = new MyList<>(myIterator.getCurrent());
		MyListIterator<E> newIterator = (MyListIterator<E>) newList.listIterator();
		for (int i = 0; i < end - start; i++){
			myIterator.next();
			newIterator.add(myIterator.getCurrent());
			newIterator.next();
		}
		return newList;
	}

	/**
	* Возвращает массив элементов
	*/
	public Object[] toArray(){
		Object[] array = new Object[size()];
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator();
		int i = 0;
		while (myIterator.hasNext()) {
			array[i++] = myIterator.next();
		}
		array[i] = myIterator.getCurrent();
		return array;
	}

	/**
	* Проверяет на пустоту
	*/
	public boolean isEmpty(){
		return head == null;
	}

	/**
	* Возвращает длину списка
	*/
	public int size(){
		if (isEmpty()){
			return 0;
		}
		else{
			MyListIterator<E> myIterator = (MyListIterator<E>) listIterator();
			int i = 1;
			while (myIterator.hasNext()){
				i++;
				myIterator.next();
			}
			return i;
		}
	}

	/**
	* Выбрасывает ошибку, если индекс находится за пределами списка
	*/
	private void checkIndex(int index){
		if (index >= size() || index < 0){
			throw new IndexOutOfBoundsException("index = " + index);
		}
	}

	/**
	* возвращает строку из перечисленных элементов
	*/
	public  String toString(){
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator();
		String str = "My list: " + myIterator.getCurrent().toString() + ", ";
		while (myIterator.hasNext()){
			myIterator.next();
			str += myIterator.getCurrent().toString() + ", ";
		}
		return str;
	}

	/**
	* Удаляет элемент по индексу
	*/
	public E remove(int index){
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(index);
		E data = myIterator.getCurrent();
		myIterator.remove();
		return data;
	}

	/**
	* Удаляет схожий с obj элемент
	*/
	public boolean remove(Object obj){
		int index = indexOf(obj);
		if (index == -1){
			return false;
		}
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(index);
		myIterator.remove();
		return true;
	}

	/**
	* Добавляет элемент по следующему индексу
	*/
	public void add(int index, E e){
		MyListIterator<E> myIterator = (MyListIterator<E>) listIterator(index);
		myIterator.add(e);
	}

	/**
	* Добавляет элемент в конец. Нельзя добавлять null
	*/
	public boolean add(E e){
		if (e == null){
			return false;
		}
		add(size()-1, e);
		return true;
	}

	public Iterator iterator(){
		throw new UnsupportedOperationException("Iterator iterator()");
	}
	public Object[] toArray(Object[] arr){
		throw new UnsupportedOperationException("Object[] toArray(Object[] arr)");
	}
	public Object set(int index, Object obj){
		throw new UnsupportedOperationException("Object set(int index, Object obj)");
	}
	public void clear(){
		throw new UnsupportedOperationException("void clear()");
	}
	public boolean retainAll(Collection ex){
		throw new UnsupportedOperationException("boolean retainAll(Collection ex)");
	}
	public boolean removeAll(Collection ex){
		throw new UnsupportedOperationException("boolean removeAll(Collection ex)");
	}
	public 	boolean addAll(int index, Collection c){
		throw new UnsupportedOperationException("boolean addAll(int index, Collection c)");
	}
	public 	boolean addAll(Collection c){
		throw new UnsupportedOperationException("boolean addAll(Collection c)");
	}







	class MyListIterator<E> implements ListIterator<E>{
		private Node<E> pointer;

		MyListIterator(Node pointer){
			this.pointer = pointer;
		}

		/**
		* Возвращает текущий элемент
		*/
		public E getCurrent(){
			if (isNotEmpty()){
				return pointer.getData();
			}
			else{
				return null;
			}
		}

		/**
		* Назначает obj текущему элементу
		*/
		public void set(E e){
			if (isNotEmpty()){
				pointer.setData(e);
			}
			else{
				throw new NullPointerException("void set(E e)");
			}
		}

		/**
		* Возвращает true, если есть ещё элементы. В противном случае возвращает false.
		*/
		public boolean hasNext( ){
			if (isNotEmpty()){
				return pointer.getNextItem() != null;
			}
			else{
				return false;
			}
		}

		/**
		* Возвращает текущий элемент. Переводит итератор на один элемент вперед
		Вызывает исключение NoSuchElementException, если не существует следующего элемента.
		*/
		public E next( ){

			if (isNotEmpty()){
				if (hasNext()){
					E data = pointer.getData();
					pointer = pointer.getNextItem();
					return data;
				}
				else{
					throw new NoSuchElementException("next");
				}
			}
			else{
				throw new NullPointerException();
			}
		}
		/**
		* Возвращает true, если есть предыдущий элемент. В противном случае возвращает false.
		*/
		public boolean hasPrevious( ){
			if (isNotEmpty()){
				return pointer.getPreviousItem() != null;
			}
			else{
				return false;
			}
		}

		/**
		* Возвращает текущий элемент. Переводит итератор на один элемент назад
		Вызывает исключение NoSuchElementException, если не существует предыдущего элемента.
		*/
		public E previous( ){

			if (isNotEmpty()){
				if (hasPrevious()){
					E data = pointer.getData();
					pointer = pointer.getPreviousItem();
					return data;
				}
				else{
					throw new NoSuchElementException("previous");
				}
			}
			else{
				throw new NullPointerException();
			}
		}

		/**
		* Вставляет obj в список перед элементом, который будет возвращен следующим вызовом next()
		*/
		public void add(E e){
			if (isNotEmpty()){
				Node<E> newNode = new Node<>(e);
				if (pointer.getNextItem() != null){
					pointer.getNextItem().setPreviousItem(newNode);
					newNode.setNextItem(pointer.getNextItem());
				}
				pointer.setNextItem(newNode);
				newNode.setPreviousItem(pointer);
			}
			else{
				throw new NullPointerException("void add(E e)");
			}
		}

		/**
		* Удаляет текущий элемент из списка
		Переходит на следующий, если есть, иначе на предыдущий
		*/
		public void remove( ){
			if (isNotEmpty()){
				if (pointer == head){
					head = head.getNextItem();
				}
				if (hasNext()){
					pointer.getNextItem().setPreviousItem(pointer.getPreviousItem());
				}
				if (hasPrevious()){
					pointer.getPreviousItem().setNextItem(pointer.getNextItem());
				}
				if (hasNext()){
					pointer = pointer.getNextItem();
				}
				else{
					pointer = pointer.getPreviousItem();
				}
			}
			else{
				throw new NullPointerException();
			}
		}

		/**
		* Возвращает индекс следующего элемента.
		Если нет следующего элемента, возвращается размер списка.
		*/
		public int nextIndex( ){
			return helperCountIndex(1);
		}

		/**
		* Возвращает индекс предыдущего элемента. Если нет предыдущего элемента, возвращается -1.
		*/
		public int previousIndex( ){
			return helperCountIndex(-1);
		}

		private int helperCountIndex(int i){
			if (isNotEmpty()){
				Node temp = pointer;
				while (temp.getPreviousItem() != null){
					temp = temp.getPreviousItem();
					i++;
				}
				return i;
			}
			else{
				throw new NullPointerException();
			}
		}

		public boolean isNotEmpty(){
			return pointer != null;
		}
	}
}
