package lab9Task1;

class Action{
  static Matrix differenceMatrices(Matrix m1, Matrix m2) throws NotGeneratedMatrixException,
                                                                DimensionMismatchException,
                                                                InCorrectMatrixException,
                                                                InvalidDimensionException{
    if (m1.getN() == m2.getN() && m1.getM() == m2.getM()){
      int n = m1.getN();
      int m = m1. getM();
      int[][] resMatrix = new int[n][m];
      int[][] matrix1 = m1.getMatrix();
      int[][] matrix2 = m2.getMatrix();
      for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
          resMatrix[i][j] = matrix1[i][j] - matrix2[i][j];
        }
      }
      return new Matrix(resMatrix);
    }
    else{
      throw new DimensionMismatchException();
    }
  }

  static Matrix sumMatrices(Matrix m1, Matrix m2) throws  NotGeneratedMatrixException,
                                                          DimensionMismatchException,
                                                          InCorrectMatrixException,
                                                          InvalidDimensionException{
    if (m1.getN() == m2.getN() && m1.getM() == m2.getM()){
      int n = m1.getN();
      int m = m1. getM();
      int[][] resMatrix = new int[n][m];
      int[][] matrix1 = m1.getMatrix();
      int[][] matrix2 = m2.getMatrix();
      for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
          resMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
        }
      }
      return new Matrix(resMatrix);
    }
    else{
      throw new DimensionMismatchException();
    }
  }
  static Matrix multiplyMatrices(Matrix m1, Matrix m2) throws NotGeneratedMatrixException,
                                                              DimensionMismatchException,
                                                              InCorrectMatrixException,
                                                              InvalidDimensionException{
    if (m1.getM() == m2.getN()){
      int m = m1.getN();
      int n = m2.getM();
      int o = m2.getN();
      int[][] resMatrix = new int[m][n];
      int[][] mA = m1.getMatrix();
      int[][] mB = m2.getMatrix();
      for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
          for (int k = 0; k < o; k++) {
            resMatrix[i][j] += mA[i][k] * mB[k][j];
          }
        }
      }
      return new Matrix(resMatrix);
    }
    else{
      throw new DimensionMismatchException();
    }
  }
}
