package lab9Task1;

class Matrix {
  private int n;
  private int m;
  private int[][] matrix;
  private boolean generationFlag;

  Matrix(int[][] matrix) throws InCorrectMatrixException{
    int n = matrix.length;
    if (n != 0){
      int m = matrix[0].length;
      for (int i = 1; i < n; i++){
        if (matrix[i].length != m){
          throw new InCorrectMatrixException();
        }
      }
      generationFlag = true;
      this.matrix = matrix;
      this.n = n;
      this.m = m;
    }
    else{
      throw new InCorrectMatrixException();
    }
  }
  Matrix(int n, int m) throws InvalidDimensionException{
    if (n > 0 && m > 0){
      this.n = n;
      this.m = m;
      generationFlag = false;
    }
    else {
      throw new InvalidDimensionException();
    }
  }
  Matrix(int n, int m, int limit) throws InvalidDimensionException{
    if (n > 0 && m > 0){
      this.n = n;
      this.m = m;
      generationFlag = false;
    }
    else {
      throw new InvalidDimensionException();
    }
    generateMatrix(limit);
    printMatrix();
  }

  int getN(){
    return n;
  }
  int getM(){
    return m;
  }
  void setN(int n) throws InvalidDimensionException{
    if (n > 0){
      generationFlag = false;
      this.n = n;
    }
    else{
      throw new InvalidDimensionException();
    }
  }
  void setM(int m) throws InvalidDimensionException{
    if (m > 0){
      generationFlag = false;
      this.m = m;
    }
    else{
      throw new InvalidDimensionException();
    }
  }
  int[][] getMatrix() throws NotGeneratedMatrixException{
    if (generationFlag){
      return matrix;
    }
    else{
      throw new NotGeneratedMatrixException();
    }
  }

  void generateMatrix(int limit){
    generationFlag = true;
    matrix = new int[n][m];
    for (int i = 0; i < n; i++){
      for (int j = 0; j < m; j++){
        matrix[i][j] = (int) (Math.random() * limit + 1);
      }
    }
  }

  void printMatrix() throws NotGeneratedMatrixException{
    if (generationFlag){
      System.out.println("Matrix:");
      for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
          System.out.print(matrix[i][j] + "\t");
        }
        System.out.println();
      }
    }
    else{
      throw new NotGeneratedMatrixException();
    }
  }

}
