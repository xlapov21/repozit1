/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lab9Task1;

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) throws NotGeneratedMatrixException,
                                                  DimensionMismatchException,
                                                  InCorrectMatrixException,
                                                  InvalidDimensionException{
      System.out.println("Началные матрицы");
      Matrix initMatrix1 = new Matrix(5, 5, 100);
      Matrix initMatrix2 = new Matrix(5, 5, 100);
      Matrix initMatrix3 = new Matrix(5, 7, 100);

      System.out.println("Сложение матриц");
      Action.sumMatrices(initMatrix1, initMatrix2).printMatrix();
      System.out.println("Вычитание матриц");
      Action.differenceMatrices(initMatrix1, initMatrix2).printMatrix();
      System.out.println("Умножени матриц");
      Action.multiplyMatrices(initMatrix1, initMatrix2).printMatrix();
    }
}
