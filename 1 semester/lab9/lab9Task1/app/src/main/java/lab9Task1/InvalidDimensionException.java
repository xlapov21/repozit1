package lab9Task1;

class InvalidDimensionException extends Exception{
  InvalidDimensionException(){
    super("Недопустимое значение размерности матрицы");
  }
}
