package lab9Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {
  @Test void MethodSumMatricesTest() throws NotGeneratedMatrixException,
                                            DimensionMismatchException,
                                            InCorrectMatrixException,
                                            InvalidDimensionException{
    int[][] m1 = new int[][]{{1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9,10,11,12}};
    int[][] m2 = new int[][]{{1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9, 1, 1, 1}};
    int[][] expectedMatrix = new int[][] {{2, 4, 6, 8},
                                          {10,12,14,16},
                                          {18,11,12,13}};
    int[][] receivedMatrix = Action.sumMatrices(new Matrix(m1), new Matrix(m2)).getMatrix();

    for (int i = 0; i < expectedMatrix.length; i++){
      for (int j = 0; j < expectedMatrix[0].length; j++){
        assertEquals(expectedMatrix[i][j],
                    receivedMatrix[i][j],
                    "Fail on i = " + i + ", j = " + j);
      }
    }
  }

  @Test void MethodDifferenceMatricesTest() throws  NotGeneratedMatrixException,
                                                    DimensionMismatchException,
                                                    InCorrectMatrixException,
                                                    InvalidDimensionException{
    int[][] m1 = new int[][]{{1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9,10,11,12}};
    int[][] m2 = new int[][]{{1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9, 1, 1, 1}};
    int[][] expectedMatrix = new int[][] {{0, 0, 0, 0},
                                          {0, 0, 0, 0},
                                          {0, 9,10,11}};
    int[][] receivedMatrix = Action.differenceMatrices(new Matrix(m1), new Matrix(m2)).getMatrix();

    for (int i = 0; i < expectedMatrix.length; i++){
      for (int j = 0; j < expectedMatrix[0].length; j++){
        assertEquals(expectedMatrix[i][j],
                    receivedMatrix[i][j],
                    "Fail on i = " + i + ", j = " + j);
      }
    }
  }

  @Test void MethodMultiplyMatricesTest() throws  NotGeneratedMatrixException,
                                                  DimensionMismatchException,
                                                  InCorrectMatrixException,
                                                  InvalidDimensionException{
    int[][] m1 = new int[][]{{1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9,10,11,12}};
    int[][] m2 = new int[][]{{1, 2},
                             {5, 6},
                             {9, 1},
                             {5, 6}};
    int[][] expectedMatrix = new int[][] {{58, 41},
                                          {138, 101},
                                          {218, 161}};
    int[][] receivedMatrix = Action.multiplyMatrices(new Matrix(m1), new Matrix(m2)).getMatrix();

    for (int i = 0; i < expectedMatrix.length; i++){
      for (int j = 0; j < expectedMatrix[0].length; j++){
        assertEquals(expectedMatrix[i][j],
                    receivedMatrix[i][j],
                    "Fail on i = " + i + ", j = " + j);
      }
    }
  }
}
