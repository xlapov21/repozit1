package lab9Task2;

class Vector{
  private int startY;
  private int startX;
  private int endY;
  private int endX;
  private int shiftX;
  private int shiftY;

  Vector(int startX, int startY, int endX, int endY){
    this.startX = startX;
    this.startY = startY;
    this.endX = endX;
    this.endY = endY;
    printVector();
  }

  Vector(int shiftX, int shiftY){
    this(0, 0, shiftX, shiftY);
  }

  int getStartX(){
    return startX;
  }
  int getStartY(){
    return startY;
  }
  int getEndX(){
    return endX;
  }
  int getEndY(){
    return endY;
  }
  int getShiftX(){
    return endX - startX;
  }
  int getShiftY(){
    return endY - startY;
  }

  void printVector(){
    System.out.println("Vektor:");
    System.out.format("Start: (%d, %d)\n", startX, startY);
    System.out.format("End: (%d, %d)\n", endX, endY);
    System.out.format("Shift: {%d, %d}\n", shiftX, shiftY);
  }

  void sum(Vector v2){
    endY += v2.getShiftY();
    endX += v2.getShiftX();
  }

  void diff(Vector v2){
    endY -= v2.getShiftY();
    endX -= v2.getShiftX();
  }

  void multiply(int a){
    endY *= a;
    endX *= a;
    }

  void div(int a){
    endY /= a;
    endX /= a;
  }

  static Vector sum(Vector v1, Vector v2){
    Vector resV = new Vector(v1.getShiftX(), v1.getShiftY());
    resV.sum(v2);
    return resV;
  }

  static Vector diff(Vector v1, Vector v2){
    Vector resV = new Vector(v1.getShiftX(), v1.getShiftY());
    resV.diff(v2);
    return resV;
  }

  static Vector multiply(Vector v1, int a){
    Vector resV = new Vector(v1.getShiftX(), v1.getShiftY());
    resV.multiply(a);
    return resV;
  }

  static Vector div(Vector v1, int a){
    Vector resV = new Vector(v1.getShiftX(), v1.getShiftY());
    resV.div(a);
    return resV;
  }
}
