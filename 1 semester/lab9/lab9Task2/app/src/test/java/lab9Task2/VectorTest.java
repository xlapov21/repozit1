package lab9Task2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;


class VectorTest {

    @Nested
    @DisplayName("Check out init vector rewrite methods ")
    class CheckOutRewriteMethods{
      Vector initV;
      Vector v2;

      @BeforeEach
      void createVectors(){
        initV = new Vector(10, 5, 7, 8);
        v2    = new Vector(6, 5, 9, 1);
      }
      @Test void MethodSumThisWithOtherVectorTest() {
        initV.sum(v2);
        assertEquals(initV.getStartX(), 10, "Не совпадают startX");
        assertEquals(initV.getStartY(), 5, "Не совпадают startY");
        assertEquals(initV.getEndX(), 10, "Не совпадают endX");
        assertEquals(initV.getEndY(), 4, "Не совпадают endY");
      }
      @Test void MethodDifferenceThisWithOtherVectorTest() {
        initV.diff(v2);
        assertEquals(initV.getStartX(), 10, "Не совпадают startX");
        assertEquals(initV.getStartY(), 5, "Не совпадают startY");
        assertEquals(initV.getEndX(), 4, "Не совпадают endX");
        assertEquals(initV.getEndY(), 12, "Не совпадают endY");
      }
    }

    @Nested
    @DisplayName("Check out sum and difference two vector methods ")
    class CheckOutMethodsSumAndDifTwoVector{
      Vector v1;
      Vector v2;

      @BeforeEach
      void createVectors(){
        v1 = new Vector(10, 5, 7, 8);
        v2 = new Vector(6, 5, 9, 1);
      }
      @Test void MethodSumTwoVectorTest() {
        Vector resV = Vector.sum(v1, v2);
        resV.printVector();
        assertEquals(resV.getShiftX(), v1.getShiftX() + v2.getShiftX(), "Не совпадает сдвиг X");
        assertEquals(resV.getShiftY(), v1.getShiftY() + v2.getShiftY(), "Не совпадает сдвиг Y");
      }
      @Test void MethodDifferenceTwoVectorTest() {
        Vector resV = Vector.diff(v1, v2);
        resV.printVector();
        assertEquals(resV.getShiftX(), v1.getShiftX() - v2.getShiftX(), "Не совпадает сдвиг X");
        assertEquals(resV.getShiftY(), v1.getShiftY() - v2.getShiftY(), "Не совпадает сдвиг Y");
      }
    }
    @Nested
    @DisplayName("Check out multiply and division vector on number methods ")
    class CheckOutMethodsMultiplyAndDivVector{
      Vector initV;

      @BeforeEach
      void createVectors(){
        initV = new Vector(10, 5, 45, 50);
      }
      @Test void MethodMultiplyVectorOnNumTest() {
        Vector resV = Vector.multiply(initV, 2);
        assertEquals(resV.getShiftX(), initV.getShiftX()*2, "Не совпадает сдвиг X");
        assertEquals(resV.getShiftY(), initV.getShiftY()*2, "Не совпадает сдвиг Y");

        assertEquals(initV.getStartX(), 10, "В начальном векторе изменен startX");
        assertEquals(initV.getStartY(), 5, "В начальном векторе изменен startY");
        assertEquals(initV.getEndX(), 45, "В начальном векторе изменен endX");
        assertEquals(initV.getEndY(), 50, "В начальном векторе изменен endY");
      }

      @Test void MethodDivisionVectorOnNumTest() {
        Vector resV = Vector.div(initV, 5);
        assertEquals(resV.getShiftX(), initV.getShiftX()/5, "Не совпадает сдвиг X");
        assertEquals(resV.getShiftY(), initV.getShiftY()/5, "Не совпадает сдвиг Y");

        assertEquals(initV.getStartX(), 10, "В начальном векторе изменен startX");
        assertEquals(initV.getStartY(), 5, "В начальном векторе изменен startY");
        assertEquals(initV.getEndX(), 45, "В начальном векторе изменен endX");
        assertEquals(initV.getEndY(), 50, "В начальном векторе изменен endY");
      }

      @Test void MethodMultiplyThisOnNumTest() {
        initV.multiply(2);
        assertEquals(initV.getStartX(), 10, "Не совпадают startX");
        assertEquals(initV.getStartY(), 5, "Не совпадают startY");
        assertEquals(initV.getEndX(), 90, "Не совпадают endX");
        assertEquals(initV.getEndY(), 100, "Не совпадают endY");
      }

      @Test void MethodDivisionThisOnNumTest() {
        initV.div(5);
        assertEquals(initV.getStartX(), 10, "Не совпадают startX");
        assertEquals(initV.getStartY(), 5, "Не совпадают startY");
        assertEquals(initV.getEndX(), 9, "Не совпадают endX");
        assertEquals(initV.getEndY(), 10, "Не совпадают endY");
      }
    }

}
