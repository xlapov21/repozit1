package lab9Task4;

class Triangle extends Polygon{

	Triangle(int[] parties){
		super(parties);
		if (parties.length != 3){
			throw new TriangleException();
		}
	}

	int getA(){
		return getPart(1);
	}

	int getB(){
		return getPart(2);
	}

	int getC(){
		return getPart(3);
	}

	@Override
	public String toString(){
		return  "Triangle: " +
						"\nPerimeter: " +
						perimeter;
	}
}
