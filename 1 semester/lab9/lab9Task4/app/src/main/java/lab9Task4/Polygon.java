package lab9Task4;

class Polygon{
	int[] parties;
	int perimeter;

	Polygon(int[] parties){
		this.parties = parties;
		countPerimetr();
		checkParties();
	}

	int getPart(int i){
		if (i <= parties.length && i > 0){
			return parties[i-1];
		}
		else{
			throw new NoPartException();
		}
	}

	void countPerimetr(){
		for (int i = 0; i < parties.length; i++){
			perimeter += parties[i];
		}
	}

	void checkParties(){
		for (int i = 0; i < parties.length; i++){
			if (2 * parties[i] >= perimeter){
				throw new NotRealPolygonException();
			}
		}
	}

	@Override
	public String toString(){
		return  "Polygon\nNumber parties: " +
						parties.length +
						"\nPerimeter: " +
						perimeter;
	}
}
