package lab9Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {
    @Test void MethodGetPartTest(){
        Polygon classUnderTest = new Polygon(new int[]{3, 4, 5, 6});
				assertEquals(classUnderTest.getPart(1), 3, "not get part");
				assertThrows(
				NoPartException.class,
				() ->{
					classUnderTest.getPart(10);
				}, "not get NoPartException");
    }

		@Test void MethodCheckPartiesTest() {
				assertThrows(
				NotRealPolygonException.class,
				() ->{
					new Polygon(new int[] {1, 2, 3, 4, 40});
				}, "not get method checkParties() in constructor not correct working");
		}
}
