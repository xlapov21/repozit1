package lab9Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {
    @Test void MethodsGettersTest(){
        Triangle classUnderTest = new Triangle(new int[]{3, 4, 5});
				assertEquals(classUnderTest.getA(), 3, "not get A");
				assertEquals(classUnderTest.getB(), 4, "not get B");
				assertEquals(classUnderTest.getC(), 5, "not get C");
    }

		@Test void ThrowsTest() {
				assertThrows(
				TriangleException.class,
				() ->{
					new Triangle(new int[] {1, 2, 3, 4});
				});
		}
}
