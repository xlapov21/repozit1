package lab9Task3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;


class WorkerTest {
	Worker worker1;

	@BeforeEach
	void createWorker1(){
		worker1 = new Worker("Ivan", "Worker", 15000);
	}

  @Test void methodEqualsTest()  throws CloneNotSupportedException{
    Worker worker2 = new Worker("Ivan", "Worker", 15000);
		Worker worker3 = new Worker("Steve", "Manager", 123);

		assertTrue(worker1.equals(worker1), "method equals() work incorrectly");
		assertTrue(worker1.equals(worker2), "method equals() does not compare identical ads");
		assertTrue(!(worker1.equals(worker3)), "method equals() work incorrectly (!)");
  }

	@Test void mithodToStringTest(){
		assertEquals(worker1.toString(), "name: Ivan\nposition: Worker\nsalary: 15000");
	}

	@Test void methodCloneTest() throws CloneNotSupportedException{
		boolean rightClone = worker1.getName() == worker2.getName() &&
												 worker1.getPosition() == worker2.getPosition() &&
												 worker1.getSalary() == worker2.getSalary();
		assertTrue(rightClone, "method clone() work incorrectly");

	}

}
