package lab9Task3;

class Worker implements Cloneable{
  private String name;
  private String position;
  private int salary;

  Worker(String name, String position, int salary){
    this.name = name;
    this.position = position;
    this.salary = salary;
  }

  String getName(){
    return name;
  }
  String getPosition(){
    return position;
  }
  int getSalary(){
    return salary;
  }
  void setPosition(String position){
    this.position = position;
  }
  void setSalary(int salary){
    this.salary = salary;
  }

  @Override
  public String toString(){
    return  "name: " + name +
            "\nposition: " + position +
            "\nsalary: " + salary;
  }
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    Worker guest = (Worker) obj;
    return  this.salary == guest.salary &&
            this.position == guest.position &&
            this.name == guest.name;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((position == null) ? 0 : position.hashCode());
    result = prime * result + salary;
    return result;
  }

  @Override
  public Worker clone() throws CloneNotSupportedException {
    return (Worker) super.clone();
  }


}
