package lab9Task3;

class Booker extends Worker{
	Booker(String name, String position, int salary){
		super(name, position, salary);
	}

	int methodBooker(int num1, int num2){
		System.out.println("I am Booker, I count numbers");
		System.out.format("%d + %d = %d\n", num1, num2, num1 + num2);
		return num1 + num2;
	}

}
