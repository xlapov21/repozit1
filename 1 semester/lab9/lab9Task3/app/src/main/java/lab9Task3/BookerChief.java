package lab9Task3;

class BookerChief extends Booker{
	BookerChief(String name, String position, int salary){
		super(name, position, salary);
	}

	void methodBookerChief(Worker obj, int num){
		System.out.println("I am Booker, I give a salary");
		obj.setSalary(num);
	}

}
