package ru.sirius.seventwo.hlapov.PerepisMatryx;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

public class PerepisMatryx {

    public static void main(String[] args) throws IOException {
        BufferedReader inputStream = null;

        try {
            inputStream =
                new BufferedReader(new FileReader("./resources/matrix1.txt"));

            String rasmernost = inputStream.readLine();
            Scanner scan = new Scanner(rasmernost);
            int n = scan.nextInt();
            int m = scan.nextInt();
            
            int[][] matrix = new int [n][m];
            String[] lines = new String[n];

            System.out.println("Начальная матрица");
            for (int i = 0; i < n; i++) {
              lines[i] = inputStream.readLine();
              System.out.println(lines[i]);
            }

            System.out.println("Конечная матрица");
            for (int i = 0; i < n; i++) {
              Scanner sc = new Scanner(lines[i]);
              for (int j = 0; j < m; j++){
                matrix[i][j] = sc.nextInt();
                System.out.print(matrix[i][j] + "\t");
              }
              System.out.println();
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

          }
    }
}
