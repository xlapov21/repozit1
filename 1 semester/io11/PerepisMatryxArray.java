package ru.sirius.seventwo.hlapov.PerepisMatryx;

import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

public class PerepisMatryxArray {

    public static void main(String[] args) throws IOException {
        BufferedReader inputStream = null;

        try {
            inputStream =
                new BufferedReader(new FileReader("./resources/matrix2.txt"));

            ArrayList<String> lines = new ArrayList<>();
            System.out.println("Начальная матрица");
            int n = 0;
            int f = 0;
            String k = inputStream.readLine();
            while (k != null) {
              lines.add(k);
              System.out.println(lines.get(n));
              n++;
              k = inputStream.readLine();
            }
            // ArrayList<ArrayList> matrix = new ArrayList<ArrayList>(n);
            System.out.println("Конечная матрица");
            for (int i = 0; i < n; i++) {
               Scanner sc = new Scanner(lines.get(i));
               while (sc.hasNextInt()) {
                 System.out.print(sc.nextInt() + "\t");
                 // (matrix.get(i)).add(j);
               }
               System.out.println();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

          }
    }
}
