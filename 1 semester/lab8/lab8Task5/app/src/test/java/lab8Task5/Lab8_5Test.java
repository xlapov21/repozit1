package lab8Task5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class Lab8_5Test {
    @Test void MethodGetIntersectionsTest() {
      Lab8_5 instance = new Lab8_5(10, 100, 100);
      instance.generateSegments();
      int nOfGetIntersections = instance.getIntersections();
      int nReal = 0;
      for (int i = 0; i < 10; i++){
        int[] pairOfSegments = instance.getPairOfSegments(i);
        if ((pairOfSegments[3] - pairOfSegments[0] >= 0) || (pairOfSegments[1] - pairOfSegments[2] <= 0)){
          nReal++;
        }
      }
      assertEquals(nReal, nOfGetIntersections);

    }
}
