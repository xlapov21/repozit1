package lab8Task5;

class Lab8_5{
  private int number;
  private int n = 0;
  private int sizeArea;
  private int offset;
  private int[] startSegment1;
  private int[] endSegment1;
  private int[] startSegment2;
  private int[] endSegment2;

  int getNumber(){
    return number;
  }
  int getSizeArea(){
    return sizeArea;
  }
  int getOffset(){
    return offset;
  }
  void setNumber(int number){
    this.number  = number;
  }
  void setSizeArea(int sizeArea){
    this.sizeArea  = sizeArea;
  }
  void setOffset(int offset){
    this.offset = offset;
  }
  Lab8_5(int number, int sizeArea, int offset){
    this.number = number;
    this.sizeArea = sizeArea;
    this.offset = offset;
  }

  void generateSegments() {
    startSegment1 = new int[number];
    endSegment1 = new int[number];
    startSegment2 = new int[number];
    endSegment2 = new int[number];
    for (int i = 0; i < number; i++){
      generateSegment(i);
    }
  }

  private void generateSegment(int i){
    if (i < number) {
      startSegment1[i] = (int) (Math.random()* sizeArea + offset);
      endSegment1[i] = (int) (Math.random()* (sizeArea - startSegment1[i] + offset)
      + startSegment1[i]);
      startSegment2[i] = (int) (Math.random()* sizeArea + offset);
      endSegment2[i] = (int) (Math.random()* (sizeArea - startSegment2[i] + offset)
      + startSegment2[i]);
    }
  }

  int getIntersections(){
    n = 0;
    for (int i = 0; i < startSegment1.length; i++){
      if ((endSegment2[i] - startSegment1[i] >= 0) || (endSegment1[i] - startSegment2[i] <= 0)){
        n++;
      }
    }
    return n;
  }
  int[] getPairOfSegments(int i){
    int[] pairOfSegments = new int[4];
    if (i < number) {
      pairOfSegments[0] = startSegment1[i];
      pairOfSegments[1] = endSegment1[i];
      pairOfSegments[2] = startSegment2[i];
      pairOfSegments[3] = endSegment2[i];
    }
    return pairOfSegments;
  }
}
