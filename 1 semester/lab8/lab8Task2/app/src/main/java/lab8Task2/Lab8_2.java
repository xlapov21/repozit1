package lab8Task2;

import java.util.Arrays;

class Lab8_2 {
  Lab8_2(long limit){
    this.limit = limit;
  }
  private long absoluteFinishTime;
  private long endTime;
  private int[] array;
  private long limit;

  long getEndTime(){
    return endTime;
  }
  long getStartTime(){
    return absoluteFinishTime - limit;
  }
  long getLimit(){
    return limit;
  }

  private void setAbsoluteFinishTime(){
    absoluteFinishTime = System.currentTimeMillis() + limit;
  }
  void generateArray(int n, int maxValue){
    setAbsoluteFinishTime();
    array = new int[n];
    for (int i = 0; i < n; i++){
      array[i] = (int) (Math.random()* maxValue);
    }
    endTime = System.currentTimeMillis();
  }

  private boolean accessByTime(){
    return absoluteFinishTime > endTime;
  }
  String arrayToString(){
    if (accessByTime()){
      return Arrays.toString(array);
    }
    else{
      return "превышено допустимое время работы программы, введите количество поменьше";
    }
  }
}
