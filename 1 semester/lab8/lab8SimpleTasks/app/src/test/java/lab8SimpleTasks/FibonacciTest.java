package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest {
    @Test void MethodFibonacciTest() {
        int[] mass = new int[] {0, 1, 1, 2, 3, 5, 8, 13};
        assertArrayEquals(Fibonacci.fibonacci(8), mass);
    }
}
