package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {
    @Test void MethodFactorialTest() {
        assertEquals(Factorial.factorial(6), 720);
    }
}
