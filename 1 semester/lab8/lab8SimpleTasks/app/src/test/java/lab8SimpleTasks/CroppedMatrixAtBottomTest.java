package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CroppedMatrixTest {
    @Test void MethodMatrixPrintCroppedTest() {
      int[] a = {1, 2, 3};
      int[] b = {4, 5, 6};
      int[] c = {7, 8, 9};
      int[][] mat = {a, b, c};
      assertEquals(CroppedMatrix.matrixPrintCropped(mat, ""), "\n4\t\n7\t8\t\n");
    }
}
