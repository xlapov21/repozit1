package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SumNaturalNumTest {
    @Test void MethodSumNTest() {
        assertEquals(SumNaturalNum.sumN(8), 36);
    }

}
