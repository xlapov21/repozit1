package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CountCharTest {
    @Test void MethodMatrixTranspTest() {
      String st = "привет, петя, как дела у твоей подруги?";
      assertEquals(CountChar.count(st, 'п'), 3);
    }
}
