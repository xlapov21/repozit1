package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixTrTest {
    @Test void MethodMatrixTranspTest() {
        int[] a = {1, 2, 3};
        int[] b = {4, 5, 6};
        int[][] mat = {a, b};

        int[] aT = {1, 4};
        int[] bT = {2, 5};
        int[] cT = {3, 6};
        int[][] matT = {aT, bT, cT};

        assertArrayEquals(MatrixTr.matrixTranspForTest(mat), matT);
    }
}
