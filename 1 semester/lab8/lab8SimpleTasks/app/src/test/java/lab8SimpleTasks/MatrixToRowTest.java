package lab8SimpleTasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixToRowTest {
    @Test void MethodMatrixPrintToRowTest() {
      int[] a = {1, 2, 3};
      int[] b = {4, 5, 6};
      int[] c = {7, 8, 9};
      int[][] mat = {a, b, c};
      assertEquals(MatrixToRow.matrixPrintToRow(mat, ""), "1, 2, 3, 4, 5, 6, 7, 8, 9, ");
    }
}
