package lab8SimpleTasks;

class FactorialRec {

  static int factorialRec(int n){
    int res = 1;
    int i = 1;
    return forFactorialRec(res, i, n);
  }

  private static int forFactorialRec(int res, int i, int n){
    return (i > n) ? res : forFactorialRec(res*i, ++i, n);
  }
}
