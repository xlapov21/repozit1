package lab8SimpleTasks;

class FibonacciRec {

  static int[] fiboRec(int n){
    if (n < 2){
      return new int[] {0};
    }
    if (n == 2) {
      return new int[] {0, 1};
    }
    int[] resmass = new int[n];
    resmass[0] = 0;
    resmass[1] = 1;
    return forFiboRec(2, resmass);
  }

  private static int[] forFiboRec(int i, int[] resmass){
    if (i == resmass.length) {
			return resmass;
		}
		else {
			resmass[i] = resmass[i - 1] + resmass[i - 2];
      i++;
			return forFiboRec(i, resmass);
    }
	}

}
