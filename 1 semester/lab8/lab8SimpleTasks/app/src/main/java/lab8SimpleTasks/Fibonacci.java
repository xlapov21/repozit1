package lab8SimpleTasks;

class Fibonacci {

	static int[] fibonacci (int n){
		if (n < 2){
			return new int[] {0};
		}
		if (n == 2) {
			return new int[] {0, 1};
		}
		int[] mass = new int[n];
		mass[1] = 1;
		for (int i = 2; i < n; i++){
			mass[i] = mass[i-1] + mass[i-2];
		}
		return mass;
	}
}
