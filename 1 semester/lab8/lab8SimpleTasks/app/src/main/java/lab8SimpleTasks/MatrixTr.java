package lab8SimpleTasks;

class MatrixTr {

	public static void main(String[] args) {

		int[][] matrix = matrixGenerator(3, 4, 10);

		System.out.println("Начальная матрица:");
		matrixPrint(matrix);
		System.out.println("Транспонированная матрица:");
		matrixPrint(matrixTranspPrint(matrix));
	}

	static int[][] matrixTranspForTest(int[][] matrix){
		int n = matrix.length;
		int m = matrix[0].length;
		int[][] matrixT = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				matrixT[i][j] = matrix[j][i];
			}
		}
		return matrixT;
	}

	static int[][] matrixTranspPrint(int[][] matrix){
		int n = matrix.length;
		int m = matrix[0].length;
		int[][] matrixT = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.println(matrix[j][i] + "\t");
			}
			System.out.println();
		}
		return matrixT;
	}

	private static void matrixPrint(int[][] matrix){
		int n = matrix.length;
		int m = matrix[0].length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print(matrix[i][j] + "\t");
			}
			System.out.println();
		}
	}


	private static int[][] matrixGenerator(int n, int m,int limit){
		int[][] matrix = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				matrix[i][j] = (int) (Math.random() * limit);
			}
		}
		return matrix;
	}

}
