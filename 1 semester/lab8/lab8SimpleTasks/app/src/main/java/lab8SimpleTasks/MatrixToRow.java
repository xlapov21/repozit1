package lab8SimpleTasks;

class MatrixToRow {
	public static void main(String[] args) {
		int[][] matrix = matrixGenerator(4, 4, 10);

	}

	static void matrixPrint(int[][] matrix, String arg){
		System.out.println(arg);
		int n = matrix.length;
		int m = matrix[0].length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print(matrix[i][j] + "\t");
			}
			System.out.println();
		}
	}

	static int[][] matrixGenerator(int n, int m,int limit){
		int[][] matrix = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				matrix[i][j] = (int) (Math.random() * limit);
			}
		}
		return matrix;
	}

	static String matrixPrintToRow(int[][] matrix, String arg){
		System.out.println(arg);
		String res = "";
		int n = matrix.length;
		int m = matrix[0].length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				res += matrix[i][j] + ", ";
			}
		}
		System.out.println(res);
		return res;
	}
}
