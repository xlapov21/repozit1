package lab8SimpleTasks;

class CountChar {

	static int count( String st, char k) {
		int res = 0;
		for (int i = 0; i < st.length(); i++){
			if (k == st.charAt(i)) {
				res++;
			}
		}
		return res;
	}
}
