package lab8Task6;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class Lab8_6Test {
  int[][] multiplyMatrices(int[][] mA, int[][] mB){
    int m = mA.length;
    int n = mB[0].length;
    int o = mB.length;
    int[][] resultArray = new int[m][n];

    for (int i = 0; i < m; i++) {
       for (int j = 0; j < n; j++) {
           for (int k = 0; k < o; k++) {
               resultArray[i][j] += mA[i][k] * mB[k][j];
           }
       }
    }
    return resultArray;
  }
    @Test void MethodMultiplyMatricesTest() {
        Lab8_6 classUnderTest = new Lab8_6(100, 100);
        classUnderTest.generateMatrices(10);
        classUnderTest.multiplyMatrices();
        int[][] resultArray = classUnderTest.getMatrix(0);
        for (int i = 1; i < 100; i++){
          resultArray = multiplyMatrices(resultArray, classUnderTest.getMatrix(i));
        }
        boolean flag = true;
        for (int i = 1; i < 100; i++){
          for (int j = 1; j < 100; j++){
            if (resultArray[i][j] == (classUnderTest.getResultArray())[i][j]){
              flag = false;
              break;
            }
          }
        }
        assertTrue(flag);
    }
}
