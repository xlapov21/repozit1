package lab8Task6;
class StrMsgException extends Exception{
  StrMsgException(String msg){
    super(msg);
  }

}
class Lab8_6{
  private int num;
  private int n;
  private int[][][] arrayMatrices;
  private int[][] resultArray;
  private long multiplicationTime;
  private boolean multiplicationFlag;
  private boolean generationFlag;

  long getMultiplicationTime() throws StrMsgException{
    if (multiplicationFlag){
      return multiplicationTime;
    }else {
      throw new StrMsgException("Матрицы не перемножены");
    }
  }
  int[][] getResultArray()  throws StrMsgException{
    if (multiplicationFlag){
      return resultArray;
    }else {
      throw new StrMsgException("Матрицы не перемножены");
    }
  }
  int[][] getMatrix(int i) throws StrMsgException{
    if (generationFlag){
      if (i < arrayMatrices.length && i >= 0){
        return arrayMatrices[i];
      }
      else{
        throw new StrMsgException("Несуществующий индекс матрицы");
      }
    }
    else{
      throw new StrMsgException("Матрицы не сгенерированы");
    }
  }
  int getNum(){
    return num;
  }
  int getN(){
    return n;
  }
  void setNum(int num){
    this.num = num;
    generationFlag = false;
    multiplicationFlag = false;
  }
  void setN(int n){
    this.n = n;
    generationFlag = false;
    multiplicationFlag = false;
  }

  Lab8_6(int num, int n){
    this.num = num;
    this.n = n;
    generationFlag = false;
    multiplicationFlag = false;
  }

  void generateMatrices(int limit){
    generationFlag = true;
    multiplicationFlag = false;
    arrayMatrices = new int[num][n][n];
    for (int i = 0; i < num; i++){
      for (int j = 0; j < n; j++){
        for (int k = 0; k < n; k++){
          arrayMatrices[i][j][k] = (int) (Math.random()*limit);
        }
      }
    }
  }

  void multiplyMatrices() throws StrMsgException{
    if (generationFlag){
      multiplicationFlag = true;
      long time1 = System.currentTimeMillis();
      resultArray = arrayMatrices[0];
      for (int i = 0; i < num; i++){
        multiplyAndWriteInResultArray(i);
      }
      multiplicationTime = System.currentTimeMillis() - time1;
    }else {
      throw new StrMsgException("Матрицы не сгенерированы");
    }
  }

  private void multiplyAndWriteInResultArray(int numberMatrix){
    int[][] intermediateMatrix = new int[n][n];
    for (int i = 0; i < n; i++){
      for (int j = 0; j < n; j++){
      intermediateMatrix[i][j] = resultArray[i][j];
      }
    }
    for (int i = 0; i < n; i++){
      for (int j = 0; j < n; j++){
        resultArray[i][j] = multiplyElementsInArrays(intermediateMatrix[i], pillarArray(arrayMatrices[numberMatrix], j));
      }
    }
  }

  private int[] pillarArray(int[][] matrix, int numberPillar){
    int[] resArray = new int[n];
    for (int i = 0; i < n; i++){
      resArray[i] = matrix[i][numberPillar];
    }
    return resArray;
  }

  private int multiplyElementsInArrays(int[] m1, int[] m2){
    int resElement = 0;
    for (int i = 0; i < n; i++){
      resElement += m1[i]*m2[i];
    }
    return resElement;
  }
}
