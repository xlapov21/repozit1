package lab8Task7;

import java.util.Scanner;

class ChildrensController extends Controller{

  Node doSmth(Node currentNode, int answer) throws StrMsgException{
    return (currentNode.getChildrens())[answer];
  }
  String[] getArrayOptions(Node currentNode) throws StrMsgException{
    return currentNode.getChildrensTitle();
  }
}
