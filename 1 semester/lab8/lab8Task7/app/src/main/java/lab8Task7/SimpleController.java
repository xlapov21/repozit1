package lab8Task7;

import java.util.Scanner;

class SimpleController{


  int inputNum(){
    Scanner in = new Scanner(System.in);
    boolean flag = true;
    int answer = 0;

    while (flag){
      if (in.hasNextInt()){
        answer = in.nextInt();
        flag = false;
      }
      else{
        in.nextLine();
        UtilityHelper.printStrMsg("Вы ввели не число");
      }
    }
    return answer;
  }
}
