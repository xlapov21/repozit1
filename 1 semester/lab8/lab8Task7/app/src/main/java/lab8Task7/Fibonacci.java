package lab8Task7;

class Fibonacci extends Action{
  SimpleController controller = new SimpleController();

  void doSmth(){
    UtilityHelper.printStrMsg("Введите число");
    int n = controller.inputNum();

    UtilityHelper.printStrMsg("n-ое число Фибоначчи = " + fibonacci(n));
  }
  String getTitle(){
    return "посчитать n-ое число Фибоначчи";
  }
  private long fibonacci(int n){
    long a = 0;
    long b = 1;
    long res = 1;
    for (int i = 1; i < n; i++){
      res = a + b;
      a = b;
      b = res;
    }
    return res;
  }
}
