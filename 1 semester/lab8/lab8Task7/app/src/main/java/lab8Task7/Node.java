package lab8Task7;

class Node{
  private Node[] childrens;
  private String title;
  private Node parent;
  private Action[] actions;

  Node(String title, Action[] actions){
    this.title = title;
    this.actions = actions;
  }
  Node(String title, Action action){
    this.title = title;
    actions = new Action[] {action};
  }
  Node(String title){
    this.title = title;
  }
  void setParent(Node parent) {
    this.parent = parent;
  }
  void setActions(Action[] activityAcces){
    this.actions = actions;
  }
  void setChildrens(Node[] childrens){
    this.childrens = childrens;
  }
  Action[] getActions(){
    return actions;
  }
  Node getParent(){
    return parent;
  }
  Node[] getChildrens(){
    return childrens;
  }
  String getTitle(){
    return title;
  }

  boolean havingChildrens(){
    return childrens != null;
  }
  boolean havingActions(){
    return actions != null;
  }
  String[] getChildrensTitle() throws StrMsgException{
    if (havingChildrens()){
      String[] resArray = new String[childrens.length];
      for (int i = 0; i < childrens.length; i++){
        resArray[i] = childrens[i].getTitle();
      }
      return resArray;
    }
    else{
      throw new StrMsgException("Нет детей");
    }
  }
  String[] getActionsTitle() throws StrMsgException{
    if (havingActions()){
      String[] resArray = new String[actions.length];
      for (int i = 0; i < actions.length; i++){
        resArray[i] = actions[i].getTitle();
      }
      return resArray;
    }
    else{
      throw new StrMsgException("Нет действий");
    }
  }
}
