package lab8Task7;

import java.util.Scanner;

class GeneralController extends Controller{

  ActionController actionController = new ActionController();
  ChildrensController childrensController = new ChildrensController();

  Node doSmth(Node currentNode, int answer) throws StrMsgException{
    switch (answer){
      case 0:
        currentNode = currentNode.getParent();
        break;
      case 1:
        actionController.inputAnswer( currentNode,
                                      true,
                                      currentNode.havingActions(),
                                      "Нет действий на этом уровне");
        break;
      case 2:
        currentNode = childrensController.inputAnswer(currentNode,
                                                      true,
                                                      currentNode.havingChildrens(),
                                                      "Нет нижних уровней");
        break;
    }
    return currentNode;

  }
  String[] getArrayOptions(Node currentNode) throws StrMsgException{
    return new String[]{"Перейти на предыдущий уровень",
                        "Выбор действия",
                        "Выбор нижнего уровня"};
  }
  void inputClose(){
    actionController.inputClose();
    childrensController.inputClose();
    in.close();
  }
}
