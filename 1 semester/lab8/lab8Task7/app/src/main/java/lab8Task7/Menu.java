package lab8Task7;

class Menu{
  Node createInstances(){

    Node level1 = new Node("Level1");
    Node level2 = new Node("Level2", new Fibonacci());
    Node level2_2 = new Node("Level2_2", new Action[]{new PrintHello(),
                                                      new SumTwoNums()});
    Node level3 = new Node("Level3", new PrintHello());

    level2.setParent(level1);
    level2_2.setParent(level1);
    level3.setParent(level2);

    level1.setChildrens(new Node[] {level2, level2_2});
    level2.setChildrens(new Node[] {level3});
    return level1;
  }

}
