package lab8Task7;

import java.util.Scanner;

abstract class Controller{

  Scanner in = new Scanner(System.in);

  Node  inputAnswer(Node currentNode,
                    boolean havePreviousMenu,
                    boolean haveActivity,
                    String msgIfNotHaveActivity) throws StrMsgException{

    if (haveActivity){
      boolean shouldContinueInputCycle = true;
      int answer = 0;
      while (shouldContinueInputCycle){
        UtilityHelper.printLevel(currentNode.getTitle());
        if (havePreviousMenu){
          UtilityHelper.printStrMsg("-1 - перейти в предыдущее меню");
        }
        UtilityHelper.printOptions(getArrayOptions(currentNode));
        if (in.hasNextInt()){
          answer = in.nextInt();
          if (havePreviousMenu && answer == -1 ){
            shouldContinueInputCycle = false;
          }
          else {
            if (answer < getArrayOptions(currentNode).length){
              shouldContinueInputCycle = false;
              UtilityHelper.printStrMsg(""+ answer);
              currentNode = doSmth(currentNode, answer);
            }
            else{
              UtilityHelper.printError();
            }
          }
        }
        else{
          UtilityHelper.printStrMsg(in.nextLine());
          UtilityHelper.printStrMsg("Вы ввели не число");
        }
      }
    }
    else{
      UtilityHelper.printStrMsg(msgIfNotHaveActivity);
    }
    return currentNode;
  }
  abstract Node doSmth(Node currentNode, int answer)  throws StrMsgException;
  abstract String[] getArrayOptions(Node currentNode) throws StrMsgException;
  void inputClose(){
    in.close();
  }

}
