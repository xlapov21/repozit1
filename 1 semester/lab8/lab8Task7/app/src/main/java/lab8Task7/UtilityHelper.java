package lab8Task7;

class UtilityHelper{

  static void printGreetings(){
    System.out.println("Привет!");
    System.out.println("Для выбора действия нажми одну из предложенных цифр");
  }
  static void printError(){
    System.out.println("Ошибка!\nТакого варианта нет\n");
  }
  static void printOptions(String[] options){
    System.out.println("Варианты:");
    for (int i = 0; i < options.length; i++){
      System.out.println(i + " - "+ options[i]);
    }
  }
  static void printLevel(String msg){
    System.out.println("Вы находитесь на уровне - " + msg + "\n");
  }
  static void printStrMsg(String msg){
    System.out.println(msg + "\n");
  }
}
