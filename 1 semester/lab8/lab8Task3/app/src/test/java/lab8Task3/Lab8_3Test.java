package lab8Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;

class Lab8_3Test {
    @Test void MethodCreateSortedArrayTest() {
        int[] initMass = {1, 3, 6, 2, 4, 9, 7, 5, 8};
        int[] initMassCopy = new int[initMass.length];
        for (int i = 0; i < initMass.length; i++) {
          initMassCopy[i] = initMass[i];
        }
        int[] sortMass = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Lab8_3 classUnderTest = new Lab8_3();
        assertArrayEquals(sortMass, classUnderTest.ceateSortedArray(initMass), "Метод не вернул отсортированный список");
        assertArrayEquals(initMassCopy, initMass, "Изменение начального массива");
    }
    @Test void MethodSortTest() {
        int[] initMass = {1, 3, 6, 2, 4, 9, 7, 5, 8};
        int[] sortMass = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Lab8_3 classUnderTest = new Lab8_3();
        classUnderTest.sort(initMass);
        assertArrayEquals(sortMass, initMass, "Массив не отсортировался");
    }
}
