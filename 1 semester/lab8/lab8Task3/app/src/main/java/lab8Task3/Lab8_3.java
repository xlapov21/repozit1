package lab8Task3;

class Lab8_3 {

  static void sort(int[] myArray) {
    quickSort(myArray, 0, myArray.length - 1);
  }

  static int[] createSortedArray(int[] myArray) {
    int[] newArray = new int[myArray.length];
    for (int i = 0; i < myArray.length; i++){
      newArray[i] = myArray[i];
    }
    quickSort(newArray, 0, newArray.length - 1);
    return newArray;
  }

  private static void quickSort(int[] source, int leftBorder, int rightBorder) {
    int leftMarker = leftBorder;
    int rightMarker = rightBorder;
    int pivot = source[(leftMarker + rightMarker) / 2];
    do {
      while (source[leftMarker] < pivot) {
        leftMarker++;
      }
      while (source[rightMarker] > pivot) {
        rightMarker--;
      }
      if (leftMarker <= rightMarker) {
        if (leftMarker < rightMarker) {
          int tmp = source[leftMarker];
          source[leftMarker] = source[rightMarker];
          source[rightMarker] = tmp;
        }
        leftMarker++;
        rightMarker--;
      }
    } while (leftMarker <= rightMarker);

    if (leftMarker < rightBorder) {
        quickSort(source, leftMarker, rightBorder);
    }
    if (leftBorder < rightMarker) {
        quickSort(source, leftBorder, rightMarker);
    }
  }

}
