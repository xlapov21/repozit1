package lab8Task1;

import java.util.Scanner;

class Lab8_1 {
  String scanIntInTerminal(){
    Scanner ch = new Scanner(System.in);
    System.out.print("Введите число: ");
    String i = "";
    if (ch.hasNextInt()){
      i += ch.nextInt();
      System.out.println("Введеное число = " + i);
    }
    else {
      System.out.println("Вы ввели не число");
    }
    return i;
  }

  String scanIntInArg(String arg){
    Scanner ch = new Scanner(arg);
    System.out.print("Введите число: ");
    String i = "";
    if (ch.hasNextInt()){
      i += ch.nextInt();
      System.out.println("Введеное число = " + i);
    }
    else {
      System.out.println("Вы ввели не число");
    }
    return i;
  }
}
