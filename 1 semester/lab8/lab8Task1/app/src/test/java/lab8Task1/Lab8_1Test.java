package lab8Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class Lab8_1Test {
    @Test void MethodScanIntInArgTest() {
        Lab8_1 classUnderTest = new Lab8_1();
        // assertDoesNotThrow(classUnderTest.scanIntInArg("r34"), "Нет метода"); ?
        assertEquals(classUnderTest.scanIntInArg("r34"), "", "Неправильно читает строку");
        assertEquals(classUnderTest.scanIntInArg("-34"), "-34", "Не понимает '-'");
        assertEquals(classUnderTest.scanIntInArg("+34"), "34", "Не понимает '+'");
    }
}
