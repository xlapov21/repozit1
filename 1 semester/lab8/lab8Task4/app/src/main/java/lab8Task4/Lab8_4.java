package lab8Task4;

import java.lang.Thread;

class Lab8_4{
  private double threshold;
  private double coefficient;
  private long maxTimeGeneration;

  double getThreshold(){
    return threshold;
  }
  double getCoefficient(){
    return coefficient;
  }
  long getMaxTimeGeneration(){
    return maxTimeGeneration;
  }
  void setThreshold(double threshold){
    this.threshold  = threshold;
  }
  void setCoefficient(double coefficient){
    this.coefficient  = coefficient;
  }
  void setMaxTimeGeneration(long maxTimeGeneration){
    this.maxTimeGeneration = maxTimeGeneration;
  }

  Lab8_4(double threshold, double coefficient, long maxTimeGeneration){
    this.threshold  = threshold;
    this.coefficient  = coefficient;
    this.maxTimeGeneration = maxTimeGeneration;
  }

  void generateWithTrace() {

    long startTime = System.currentTimeMillis();
    long absolutFinishTime = startTime + maxTimeGeneration;
    double resultProcess;
    do {
      resultProcess = generateNumber();
      if (resultProcess > threshold){
        break;
      }
      randomDelay(500);
    } while (System.currentTimeMillis() <= absolutFinishTime);

    System.out.println((System.currentTimeMillis() - startTime) + " : " + resultProcess);
  }

  private void randomDelay(int limit){
    int randomTime = (int) (Math.random() * limit);
    try {
      Thread.sleep(randomTime);
    }catch(InterruptedException e) {}
  }

  private double generateNumber(){
    return (double) (Math.random() * 2*threshold*coefficient - threshold*coefficient);
  }
}
