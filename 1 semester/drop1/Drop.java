// package ru.sirius.seventwo.hlapov.dropReadWrite;
package drop1;

import java.io.*;

public class Drop {

    private String message = "";
    private boolean empty = true;

    public synchronized String take() throws InterruptedException{

        while (empty) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        empty = true;
        notifyAll();
        System.out.println("Читаю");
        String fail = read();
        write("");
        return fail;
    }

    public synchronized void put(String message) throws IOException{
        while (!empty) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        empty = false;

        System.out.println("Пишу");
        write(message);
        notifyAll();
    }

    public String read() {
      try (BufferedReader reader = new BufferedReader(new FileReader("./drop1/forDrop.txt"))) {
        String line;
        while ((line = reader.readLine()) != null) {
          message += line;
        }
      }
      catch (IOException e) {
           e.printStackTrace();
      }
      String o = message;
      message = "";
      return o;
    }

    public void write(String mess) {
      try(FileWriter writer = new FileWriter("./drop1/forDrop.txt", false))
      {
          writer.write(mess);

          writer.flush();
      }
      catch(IOException ex){
          System.out.println(ex.getMessage());
      }
    }
}
