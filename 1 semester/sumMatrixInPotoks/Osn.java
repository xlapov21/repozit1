package ru.sirius.seventwo.hlapov.matPotoks;

import java.util.Date;

class Osn {

  public static void main(String[] args) {
    System.out.println("sposob1:");
    sposob1();
    System.out.println("sposob2:");
    sposob2();
  }
  //Способ 1
  public static class Mythread extends Thread {
    Matrix mat1;
    Matrix mat2;
    Matrix rezult;
    Mythread(Matrix mat1, Matrix mat2){
      this.mat1 = mat1;
      this.mat2 = mat2;
    }
    public void run(){
      NDelK t = new NDelK();
      this.rezult = t.sumMatrix(this.mat1, this.mat2);
    }
    public Matrix getRezult(){
      return this.rezult;
    }
  }
  public static void sposob1(){
    NDelK methods = new NDelK();
    int nForMatrixes = 15000;
    int mForMatrixes = 15000;
    int kolThreads = 10;
    Matrix initialMatrix1 = new Matrix(nForMatrixes, mForMatrixes);
    Matrix initialMatrix2 = new Matrix(nForMatrixes, mForMatrixes);

    Matrix[] rasdelInitMat1 = methods.delMatrix(initialMatrix1, kolThreads);
    Matrix[] rasdelInitMat2 = methods.delMatrix(initialMatrix2, kolThreads);
    Mythread[] potok = new Mythread[kolThreads];
    Date time1 = new Date();
    for (int i = 0; i < kolThreads; i++){
      potok[i] = new Mythread(rasdelInitMat1[i], rasdelInitMat2[i]);
      potok[i].start();
    }

    try {
      for (int i = 0; i < kolThreads; i++){
        potok[i].join();
      }
    } catch (InterruptedException e) {}

      Matrix slepka = new Matrix((potok[0].getRezult()).array);
      for (int i = 1; i < kolThreads; i++){
        slepka = methods.unificationMatrix(slepka, potok[i].getRezult());
      }
      Date time2 = new Date();
      System.out.println(time2.getTime() - time1.getTime());
  }
  //Способ 2
  public static class Mythread2 extends Thread {
    int[][] mat1;
    int[][] mat2;
    int[][] rezult;
    int nach;
    int size;
    Mythread2(int[][] mat1, int[][] mat2, int nach, int size){
      this.mat1 = mat1;
      this.mat2 = mat2;
      this.nach = nach;
      this.size = size;
    }
    public void run(){
      NDelK t = new NDelK();
      this.rezult = t.sumMassInMass(this.mat1, this.mat2, nach, size);
    }
    public int[][] getRezult(){
      return this.rezult;
    }
  }
  public static void sposob2(){
    NDelK methods = new NDelK();
    int nForMatrixes = 15000;
    int mForMatrixes = 15000;
    int kolThreads = 10;
    Matrix initialMatrix1 = new Matrix(nForMatrixes, mForMatrixes);
    Matrix initialMatrix2 = new Matrix(nForMatrixes, mForMatrixes);
    int[][] mass1 = initialMatrix1.array;
    int[][] mass2 = initialMatrix2.array;


    Mythread2[] potok = new Mythread2[kolThreads];
    int[] sizeForPotoks = methods.delenie(nForMatrixes, kolThreads);
    int nachForPotocs = 0;

    Date time1 = new Date();
    for (int i = 0; i < kolThreads; i++){
      potok[i] = new Mythread2(
        mass1, mass2, nachForPotocs, sizeForPotoks[i]);
      potok[i].start();
        nachForPotocs += sizeForPotoks[i];
    }

    try {
      for (int i = 0; i < kolThreads; i++){
        potok[i].join();
      }
    } catch (InterruptedException e) {
      System.out.println(e);
    }

    int[][] slepkaMass = potok[0].getRezult();
    for (int i = 1; i < kolThreads; i++){
      slepkaMass = methods.unificationArray(slepkaMass, potok[i].getRezult());
    }

    Matrix slepka = new Matrix(slepkaMass);
    Date time2 = new Date();
    System.out.println(time2.getTime() - time1.getTime());
  }

}
