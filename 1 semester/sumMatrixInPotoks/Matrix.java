package ru.sirius.seventwo.hlapov.matPotoks;

class Matrix implements Serializable{
  int n;
  int m;
  int[][] array;
  private static final long SerialVersionUID = 1L;

  Matrix(){
    this.n = (int) (Math.random()*20 + 2);
    this.m = (int) (Math.random()*10 + 1);

    napolnArray();
  }
  Matrix(int n, int m) {
    this.n = n;
    this.m = m;
    napolnArray();
  }
  Matrix(int[][] array){
      this.n = array.length;
      this.m = array.length != 0 ? array[0].length : 0;
      this.array = array;
  }


  private void napolnArray(){

    this.array = new int[this.n][this.m];

    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.array[i][j] = (int) (Math.random()*100+1);
      }
    }
  }

  public void printArray(){
    System.out.println("Матрица array:");
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        System.out.print(this.array[i][j] + "\t");
      }
      System.out.println();
    }
  }

  public int[][] getArray(){
    return this.array
  }


}
