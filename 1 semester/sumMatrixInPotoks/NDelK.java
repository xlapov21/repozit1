package ru.sirius.seventwo.hlapov.matPotoks;

class NDelK {

  public static Matrix[]  delMatrix(Matrix initialMatrix, int  k){
    int n = initialMatrix.n;
    int m = initialMatrix.m;
    int[] kolK = delenie(n, k);
    Matrix[] rezult = new Matrix[k];
    int[][] arrayForRezult;

    int f = 0;
    for (int j = 0; j < k; j++) {
      arrayForRezult = new int[kolK[j]][m];
      for (int q = 0; q < kolK[j]; q++) {
        arrayForRezult[q] = initialMatrix.array[f];
        f++;
      }
      rezult[j] = new Matrix(arrayForRezult);
    }
    return rezult;
  }

  public static int[][]  delmass(int[] mass, int  k){
    int n = mass.length;
    int[] kolK = delenie(n, k);
    int[][] rezult = new int[k][(int) (Math.ceil((double) n/k))];

    int f = 0;
    for (int j = 0; j < k; j++) {
      for (int q = 0; q < kolK[j]; q++) {
        rezult[j][q] = mass[f];
        f++;
      }
    }
    return rezult;
  }

  public static int[] delenie(int n ,int k){
    int[] kolK = new int[k];
    int s = n/k;
    int ost = n%k;
    for (int i = 0; i < k; i++){
      kolK[i] = s;
    }
    int j = 0;
    while (ost != 0) {
      kolK[j]++;
      j++;
      ost--;
    }
    return kolK;
  }

  public static int[][] sumMassInMass(int[][] a1, int[][] a2, int nach, int size){
     int[][] result = new int[size][a1[0].length];
     for (int i = nach; i < nach + size; i++){
       for (int j = 0; j < a1[0].length; j++){
         result[i - nach][j] = a1[i][j] + a2[i][j];
       }
     }
     return result;
   }

  private  static int[][] sumPart(int[][] a1, int[][] a2){
     int[][] result = new int[a1.length][a1[0].length];
     for (int i = 0; i < a1.length; i++){
       for (int j = 0; j < a1[0].length; j++){
         result[i][j] = a1[i][j] + a2[i][j];
       }
     }
     return result;
   }
   public  static Matrix sumMatrix(Matrix m1, Matrix m2){
     Matrix result = new Matrix(m1.n, m1.m);
     if (m1.n == m2.n && m1.m == m2.m){
       result.array = sumPart(m1.array, m2.array);
     }
     else {
       System.out.println("Неодинаковые размерности матриц экземпляров");
     }
     return result;
   }
   public  static int[][] unificationArray(int[][] m1, int[][] m2){
     int[][] arrayForRezult = new int[m1.length + m2.length][m1[0].length];
       int nSchet = 0;
       for (int i = 0; i < m1.length; i++){
         arrayForRezult[nSchet] = m1[i];
         nSchet++;
       }
       for (int i = 0; i < m2.length; i++){
         arrayForRezult[nSchet] = m2[i];
         nSchet++;
       }
     return arrayForRezult;
   }

   public  static Matrix unificationMatrix(Matrix m1, Matrix m2){
     int[][] arrayForRezult = new int[m1.n + m2.n][m1.m];
     if (m1.m == m2.m){
       int nSchet = 0;
       for (int i = 0; i < m1.n; i++){
         arrayForRezult[nSchet] = m1.array[i];
         nSchet++;
       }
       for (int i = 0; i < m2.n; i++){
         arrayForRezult[nSchet] = m2.array[i];
         nSchet++;
       }
     }
     else {
       System.out.println("Неодинаковое количество столбцов матриц экземпляров");
     }
     Matrix rezult = new Matrix(arrayForRezult);
     return rezult;
   }
}
